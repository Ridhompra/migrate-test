package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"reflect"
	"strconv"
	"time"

	_ "github.com/lib/pq"
)

func FindCustomQuery(db *sql.DB, ctx context.Context, query string) (map[string]interface{}, error) {
	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	data, err := getResIndex(rows)
	if err != nil {
		return nil, err
	}
	if data == nil {
		return nil, nil
	}

	return data[0], nil
}

func getResIndex(rows *sql.Rows) ([]map[string]interface{}, error) {
	lisColumn, _ := rows.Columns()
	var res []map[string]interface{}
	var sampleSliceUint8 []uint8
	for rows.Next() {
		dest := make([]interface{}, len(lisColumn))
		for i := range lisColumn {
			dest[i] = new(interface{})
		}
		err := rows.Scan(dest...)
		if err != nil {
			return nil, err
		}
		itermap := make(map[string]interface{})
		for i, colName := range lisColumn {
			value := *dest[i].(*interface{})
			switch reflect.TypeOf(value) {
			case reflect.TypeOf(time.Time{}):
				toStringFormatDate := time.Date(2012, time.February, 23, 0, 0, 0, 0, time.UTC).Format("2006-01-02")
				value = toStringFormatDate
			case reflect.TypeOf(sampleSliceUint8):
				stringValue := string(value.([]uint8))
				floatValue, _ := strconv.ParseFloat(stringValue, 64)
				value = int(floatValue)
			}
			itermap[colName] = value
		}
		res = append(res, itermap)
	}
	return res, nil
}

func ExecDBQuery(db *sql.DB, ctx context.Context, query string) (err error) {
	trx, err := db.Begin()
	if err != nil {
		return
	}

	result, err := trx.Exec(query)
	if err != nil {
		return
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return
	}

	defer commitTrx(trx, &err, rowsAffected)

	return
}
func commitTrx(trx *sql.Tx, err *error, rowsAffected int64) {
	if p := recover(); p != nil {
		trx.Rollback()
		panic(p)
	} else if *err != nil {
		trx.Rollback()
	} else {
		*err = trx.Commit()
		if *err != nil {
			*err = fmt.Errorf("error committing transaction: %w", *err)
		}
	}
}
func InitDatastore(driver, host, user, password, database string, port int) (*sql.DB, error) {
	var address string
	if driver == "postgres" {
		address = fmt.Sprintf("%s://%s:%s@%s:%d/%s?sslmode=disable", driver, user, password, host, port, database)
	} else if driver == "godror" {
		address = fmt.Sprintf(`user="%s" password="%s" 
		connectString="(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=%s)(PORT=%d))(CONNECT_DATA=(SID=%s)))"`,
			user, password, host, port, database)
	}
	db, err := sql.Open(driver, address)
	if err != nil {
		fmt.Println(err)
	}
	if err := db.Ping(); err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	return db, nil
}

func GenerateRandomMixStrInt(number int) string {
	characters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	length := number
	result := make([]byte, length)
	for i := 0; i < length; i++ {
		result[i] = characters[rand.Intn(len(characters))]
	}
	return string(result)
}

func GenerateUIDKEY() (res string) {
	res = "TEST" + GenerateRandomMixStrInt(10)
	return
}

func dummyProjectIsi(db *sql.DB, ctx context.Context) {
	project_id1 := GenerateUIDKEY()
	project_id2 := GenerateUIDKEY()

	// create table data project
	projectData := []string{
		fmt.Sprintf("('%s', 'project A', 400000000)", project_id1),
		fmt.Sprintf("('%s', 'project B', 350000000)", project_id2),
	}

	for _, data := range projectData {
		query := fmt.Sprintf("INSERT INTO project_isi (project_id, project_name, cost_project) VALUES %s", data)
		err := ExecDBQuery(db, ctx, query)
		if err != nil {
			fmt.Println("Error inserting project data:", err)
			return
		}
	}
}
func dummyCalculationEmployee(db *sql.DB, ctx context.Context) {
	var (
		employee_name               = "ridho"
		salary                      = 10_000_000
		total_work_hours_perweek    = 80
		cost_salary                 = salary / 160 * 40 / total_work_hours_perweek
		list_project                = []string{"project A", "project B"}
		total_work_hours_perproject = []int{60, 20}
		total_cost_perproject       = []int{total_work_hours_perproject[0] * cost_salary, total_work_hours_perproject[1] * cost_salary}
	)

	data := fmt.Sprintf("('%s', '%s', %d, %d, %d, '{%s, %s}', '{%d,%d}', '{%d,%d}')",
		GenerateUIDKEY(), employee_name, salary, cost_salary, total_work_hours_perweek,
		list_project[0], list_project[1], total_work_hours_perproject[0], total_work_hours_perproject[1],
		total_cost_perproject[0], total_cost_perproject[1])

	query := fmt.Sprintf(`
	INSERT INTO employee_cost 
	(employee_id, employee_name, salary, cost_salary, total_work_hours_perweek, 
		list_project, total_work_hours_perproject, total_cost_perproject) 
	VALUES %s`, data)

	err := ExecDBQuery(db, ctx, query)
	if err != nil {
		fmt.Println("Error inserting project data:", err)
		return
	}
}

var (
	PG_DB          *sql.DB
	PG_DB_HOST     = "localhost"
	PG_DB_PORT     = 5438
	PG_DB_USER     = "postgres"
	PG_DB_PASSWORD = "password"
	PG_DB_DATABASE = "calculation_salary"
	PG_DB_DRIVER   = "postgres"
)

// DATA EXAMPLE
var (
	employee_name               = "ridho"
	salary                      = 10_000_000
	total_work_hours_perweek    = 80
	cost_salary                 = salary / 160 * 40 / total_work_hours_perweek
	list_project                = []string{"project A", "project B"}
	total_work_hours_perproject = []int{30, 20}
	total_cost_perproject       = []int{total_work_hours_perproject[0] * cost_salary, total_work_hours_perproject[1] * cost_salary}
)

/*
bikin total non overtime
  - get salary employee
  - itung total kerja selama seminggu
  - lalu dihitung salary perhournya
  - list project [namaproject --total jam kerja]

employee_id, salary, total_work_hours_perweek, list_project, total_work_per_project, total_cost_per_project
*/
func main() {
	db, err := InitDatastore(PG_DB_DRIVER, PG_DB_HOST, PG_DB_USER, PG_DB_PASSWORD, PG_DB_DATABASE, PG_DB_PORT)
	if err != nil {
		log.Fatal("failed to connect DB POSTGRES!")
	}
	ctx := context.Background()

	dummyProjectIsi(db, ctx)
	dummyCalculationEmployee(db, ctx)

	query := fmt.Sprintf(`
	UPDATE employee_cost 
	SET salary = %d, cost_salary = %d, total_work_hours_perweek = %d,
		list_project = '{%s, %s}', total_work_hours_perproject = '{%d, %d}', 
		total_cost_perproject = '{%d, %d}'
	WHERE employee_name = '%s';`,
		salary, cost_salary, total_work_hours_perweek,
		list_project[0], list_project[1], total_work_hours_perproject[0], total_work_hours_perproject[1],
		total_cost_perproject[0], total_cost_perproject[1], employee_name)

	err = ExecDBQuery(db, ctx, query)
	if err != nil {
		fmt.Println("Error inserting project data:", err)
		return
	}
}
