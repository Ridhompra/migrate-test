CREATE TABLE employee_cost (
    employee_id VARCHAR(50) PRIMARY KEY,
    salary INTEGER,
    total_work_hours_perweek INTEGER,
    list_project VARCHAR(255)[],
    total_work_hours_perproject INTEGER[],
    total_cost_perproject INTEGER[],
    cost_salary INTEGER,
    employee_name VARCHAR(255)
);

CREATE TABLE project_isi (
    project_id VARCHAR(50) PRIMARY KEY,
    project_name VARCHAR(255),
    cost_project INTEGER
);
CREATE TABLE employee_isi (
    employee_id VARCHAR(50) PRIMARY KEY,
    name VARCHAR(255),
);
